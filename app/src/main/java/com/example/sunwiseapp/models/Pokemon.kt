package com.example.sunwiseapp.models

import java.util.*

data class Pokemon(
    val name: String,
    val url: String
){
    fun getNumberPokemon():Int{
        val urlSplit = url.split("/").toTypedArray()
        return urlSplit[urlSplit.size - 2].toInt()
    }

    fun getNameUpper():String{
        return name.toUpperCase(Locale.ROOT)
    }
}