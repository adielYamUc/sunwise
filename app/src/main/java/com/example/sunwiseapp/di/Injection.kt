package com.example.sunwiseapp.di

import android.content.Context
import com.example.sunwiseapp.data.local.DbDataSource
import com.example.sunwiseapp.data.remote.PokemonRemoteDataSource
import com.example.sunwiseapp.repository.PokemonDataSource
import com.example.sunwiseapp.repository.PokemonLocalRepository
import com.example.sunwiseapp.repository.PokemonRemoteRepository
import com.example.sunwiseapp.repository.user.UserDataSource
import com.example.sunwiseapp.repository.user.UserDataSourceImp
import com.example.sunwiseapp.repository.user.UserLocalRepository

object Injection {

    private val pokemonRepository = PokemonRemoteRepository()

    private lateinit var pokemonDbRepository: PokemonLocalRepository
    private lateinit var dbDataSource: DbDataSource

    private lateinit var userDbRepository: UserLocalRepository
    private lateinit var userDataSource : UserDataSource

    fun setup(context: Context){
        dbDataSource = PokemonDataSource(context) //method locals
        pokemonDbRepository = PokemonLocalRepository(dbDataSource)

        userDataSource = UserDataSourceImp(context)
        userDbRepository = UserLocalRepository(userDataSource)
    }

    fun providerDBRepository(): PokemonLocalRepository = pokemonDbRepository
    fun providerRemoteRepository(): PokemonRemoteDataSource = pokemonRepository

    fun providerUserDbRepository (): UserLocalRepository = userDbRepository

}