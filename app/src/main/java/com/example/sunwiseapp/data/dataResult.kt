package com.example.sunwiseapp.data

sealed class dataResult <out T> {
    data class Success<T>(val data: List<T>?) : dataResult<T>()
    data class Error(val exception:String?) : dataResult<Nothing>()
}