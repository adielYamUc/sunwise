package com.example.sunwiseapp.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.sunwiseapp.data.local.dto.PokemonDto

@Dao
interface PokemonDao {
    @Query("SELECT * from tb_pokemon")
    suspend fun getAllPokemon(): List<PokemonDto>

    @Insert(onConflict = REPLACE)
    suspend fun addPokemons(pokemons: List<PokemonDto>)
}