package com.example.sunwiseapp.data.local.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_user")
class UserDto (
    @PrimaryKey(autoGenerate = true) val id:Int,
    @ColumnInfo(name = "email") val email:String
)