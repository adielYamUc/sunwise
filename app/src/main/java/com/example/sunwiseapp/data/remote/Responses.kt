package com.example.sunwiseapp.data.remote

import com.example.sunwiseapp.models.Pokemon

data class PokemonResponse (
    val count:Int?,
    val next:String?,
    val previous:String?,
    val results: List<Pokemon>?
){}