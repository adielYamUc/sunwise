package com.example.sunwiseapp.data.remote

import com.example.sunwiseapp.data.dataResult
import com.example.sunwiseapp.models.Pokemon

interface PokemonRemoteDataSource {
    suspend fun getPokemos(): dataResult<Pokemon>
}