package com.example.sunwiseapp.data.local

import com.example.sunwiseapp.data.local.dto.PokemonDto
import com.example.sunwiseapp.models.Pokemon

interface DbDataSource {
    suspend fun addPokemons(pokemons:List<PokemonDto>)
    suspend fun getAllPokemon():List<PokemonDto>
}