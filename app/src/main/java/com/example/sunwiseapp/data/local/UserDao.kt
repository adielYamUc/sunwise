package com.example.sunwiseapp.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sunwiseapp.data.local.dto.UserDto

@Dao
interface UserDao {
    @Query("SELECT * from tb_user")
    suspend fun getUser(): List<UserDto>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUser(user : UserDto)

    @Query("delete from tb_user")
    suspend fun deleteAll()
}