package com.example.sunwiseapp.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.sunwiseapp.data.local.dto.PokemonDto
import com.example.sunwiseapp.data.local.dto.UserDto

@Database(entities = [PokemonDto::class, UserDto::class], version = 1)
abstract class DataBase : RoomDatabase() {
    abstract fun pokemonDao(): PokemonDao
    abstract fun userDao():UserDao

    companion object {
        private var INSTANCE: DataBase? = null
        private const val DBNAME = "Pokemon.db"

        fun getInstance(context: Context): DataBase? {
            if (INSTANCE == null) {
                synchronized(DataBase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context,
                        DataBase::class.java, DBNAME
                    )
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}