package com.example.sunwiseapp

import android.app.Application
import com.example.sunwiseapp.di.Injection

class Application : Application() {
    override fun onCreate() {
        super.onCreate()
        Injection.setup(this)
    }
}