package com.example.sunwiseapp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.example.sunwiseapp.R
import com.example.sunwiseapp.di.Injection
import com.example.sunwiseapp.viewmodel.PokemonVM
import com.example.sunwiseapp.viewmodel.PokemonVMFactory
import com.example.sunwiseapp.viewmodel.UserVM
import com.example.sunwiseapp.viewmodel.UserVMFactory
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var viewModel : UserVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setUpViewModel()
        setUpUi()
    }

    fun setUpViewModel(){
        viewModel = ViewModelProviders.of(this, UserVMFactory (Injection.providerUserDbRepository()))
            .get(UserVM::class.java)

        viewModel.userFechSuccess.observe(this, {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        })

        viewModel.fetchFail.observe(this, {
            Toast.makeText(applicationContext,it,Toast.LENGTH_LONG).show()
        })

        viewModel.isLogin.observe(this, {
            if (it){
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        })
    }

    fun setUpUi(){
        continuar.setOnClickListener {
            if (email.text.isNotEmpty() && password.text.isNotEmpty()){
                viewModel.fakeLogin(email.text.toString(), password.text.toString())
            }else{
                //empty data
                Toast.makeText(applicationContext,getText(R.string.text),Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.isLogin()
    }
}