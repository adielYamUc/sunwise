package com.example.sunwiseapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sunwiseapp.R
import com.example.sunwiseapp.models.Pokemon
import com.example.sunwiseapp.utils.Constants

class PokemonAdapter(private val list: List<Pokemon>) : RecyclerView.Adapter<PokemonAdapter.Holder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val inflater = LayoutInflater.from(parent.context)
        return Holder(inflater, parent)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

    inner class Holder (inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_pokemon, parent, false)){

        private var name: TextView? = null
        private var image : ImageView? = null

        init {
            name = itemView.findViewById(R.id.name)
            image = itemView.findViewById(R.id.image)
        }

        fun bind (pokemon : Pokemon){
            name?.text = pokemon.getNameUpper()
            image?.let {
                Glide.with(itemView.context)
                    .load(Constants.PATH_IMG + pokemon.getNumberPokemon() + ".png")
                    .into(it)
            }
        }

    }

}