package com.example.sunwiseapp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sunwiseapp.R
import com.example.sunwiseapp.di.Injection
import com.example.sunwiseapp.ui.adapter.PokemonAdapter
import com.example.sunwiseapp.viewmodel.PokemonVM
import com.example.sunwiseapp.viewmodel.PokemonVMFactory
import com.example.sunwiseapp.viewmodel.UserVM
import com.example.sunwiseapp.viewmodel.UserVMFactory
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel : PokemonVM
    private lateinit var userViewModel : UserVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpViewModel()
        setUpUi()
    }

    private fun setUpViewModel(){

        viewModel = ViewModelProviders.of(this, PokemonVMFactory (
            Injection.providerRemoteRepository(),
            Injection.providerDBRepository())
        ).get(PokemonVM::class.java)

        userViewModel = ViewModelProviders.of(this, UserVMFactory (
            Injection.providerUserDbRepository())).get(UserVM::class.java)

        viewModel.getLocalPokemon()

        viewModel.pokemonFechSuccess.observe(this, {
            list_pokemons.apply {
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = PokemonAdapter(it)
            }
        })

        viewModel.fetchFail.observe(this, {

        })

        userViewModel.userFechSuccess.observe(this, {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        })
    }

    private fun setUpUi(){

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                userViewModel.logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}