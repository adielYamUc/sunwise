package com.example.sunwiseapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sunwiseapp.data.dataResult
import com.example.sunwiseapp.data.remote.PokemonRemoteDataSource
import com.example.sunwiseapp.models.Pokemon
import com.example.sunwiseapp.repository.PokemonLocalRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PokemonVM (
    private val remoteRepository : PokemonRemoteDataSource,
    private val localRepository: PokemonLocalRepository)
    : ViewModel() {

    private val _pokemonFetchSuccess = MutableLiveData<List<Pokemon>>()
    val pokemonFechSuccess : LiveData<List<Pokemon>> = _pokemonFetchSuccess

    private val _fetchFail = MutableLiveData<String>()
    val fetchFail : LiveData<String> = _fetchFail

    fun getRemotePokemon(){
        viewModelScope.launch {
            val response : dataResult<Pokemon> = withContext(Dispatchers.IO){
                remoteRepository.getPokemos()
            }

            when (response){
                is dataResult.Success ->{
                    withContext(Dispatchers.IO){
                        response.let {
                            localRepository.addPokemons(it.data!!)
                            _pokemonFetchSuccess.postValue(it.data)
                        }
                    }
                }

                is dataResult.Error ->{
                    withContext(Dispatchers.IO){
                        response.let {
                            _fetchFail.postValue(it.exception)
                        }
                    }
                }
            }
        }
    }

    fun getLocalPokemon(){
        viewModelScope.launch {
            localRepository.getAllPokemon().let {
                if (it.isNotEmpty()){
                    _pokemonFetchSuccess.postValue(it)
                }else{
                    getRemotePokemon()
                }
            }
        }
    }

}