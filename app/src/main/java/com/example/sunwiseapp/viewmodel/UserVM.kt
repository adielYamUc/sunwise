package com.example.sunwiseapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sunwiseapp.models.Pokemon
import com.example.sunwiseapp.repository.PokemonLocalRepository
import com.example.sunwiseapp.repository.user.UserLocalRepository
import com.example.sunwiseapp.utils.Constants
import kotlinx.coroutines.launch

class UserVM (private val localRepository: UserLocalRepository): ViewModel() {

    private val _userFetchSuccess = MutableLiveData<String>()
    val userFechSuccess : LiveData<String> = _userFetchSuccess

    private val _fetchFail = MutableLiveData<String>()
    val fetchFail : LiveData<String> = _fetchFail

    private val _isLogin = MutableLiveData<Boolean>()
    val isLogin : LiveData<Boolean> = _isLogin

    fun fakeLogin(email:String, pass:String){
        viewModelScope.launch {
            if (email == Constants.FAKE_EMAIL && pass == Constants.FAKE_PASS){
                localRepository.saveUser(email)
                _userFetchSuccess.postValue("Ok")
            }else{
                _fetchFail.postValue("Something wrong!")
            }
        }
    }

    fun logout (){
        viewModelScope.launch {
            localRepository.deleteUser()
            _userFetchSuccess.postValue("Ok")
        }
    }

    fun isLogin(){
        viewModelScope.launch {
            _isLogin.postValue(localRepository.getUser().isNotEmpty())
        }
    }

}