package com.example.sunwiseapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sunwiseapp.data.remote.PokemonRemoteDataSource
import com.example.sunwiseapp.repository.PokemonLocalRepository

class PokemonVMFactory (
    private val remoteRepository: PokemonRemoteDataSource,
    private val localRepository: PokemonLocalRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PokemonVM(remoteRepository, localRepository) as T
    }
}