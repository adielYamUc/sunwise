package com.example.sunwiseapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sunwiseapp.repository.user.UserLocalRepository

class UserVMFactory(
    private val localRepository: UserLocalRepository
): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UserVM(localRepository) as T
    }

}