package com.example.sunwiseapp.repository

import com.example.sunwiseapp.data.dataResult
import com.example.sunwiseapp.data.remote.ApiClient
import com.example.sunwiseapp.data.remote.PokemonRemoteDataSource
import com.example.sunwiseapp.models.Pokemon
import java.lang.Exception

class PokemonRemoteRepository: PokemonRemoteDataSource {
    override suspend fun getPokemos(): dataResult<Pokemon> {
        try {
            ApiClient.build()?.getPokemon()?.let {
                if (it.isSuccessful){
                    return dataResult.Success(it.body()?.results)
                }
                return dataResult.Error("Error ${it.code()}")
            } ?:run {
                return dataResult.Error("Fail")
            }
        }catch (ex:Exception){
            return dataResult.Error(ex.localizedMessage)
        }
    }
}