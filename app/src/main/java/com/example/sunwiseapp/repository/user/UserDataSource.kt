package com.example.sunwiseapp.repository.user

import com.example.sunwiseapp.data.local.dto.UserDto

interface UserDataSource {
    suspend fun saveUser(user:UserDto)
    suspend fun getUser(): List<UserDto>
    suspend fun deleteUser()
}