package com.example.sunwiseapp.repository

import com.example.sunwiseapp.data.local.DbDataSource
import com.example.sunwiseapp.data.local.dto.PokemonDto
import com.example.sunwiseapp.models.Pokemon

class PokemonLocalRepository (private val dataSource: DbDataSource) {

    suspend fun getAllPokemon() : List<Pokemon> {
        return dataSource.getAllPokemon().map {
            Pokemon(
                it.name,
                it.path
            )
        }
    }

    suspend fun addPokemons(pokemonsList: List<Pokemon>) {
        dataSource.addPokemons(pokemonsList.map {
            PokemonDto(0, it.name, it.url)
        })
    }

}