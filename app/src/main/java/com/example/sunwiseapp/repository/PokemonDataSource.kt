package com.example.sunwiseapp.repository

import android.content.Context
import com.example.sunwiseapp.data.local.DataBase
import com.example.sunwiseapp.data.local.DbDataSource
import com.example.sunwiseapp.data.local.PokemonDao
import com.example.sunwiseapp.data.local.dto.PokemonDto
import com.example.sunwiseapp.models.Pokemon

class PokemonDataSource (context: Context) : DbDataSource {

    private lateinit var pokemonDao: PokemonDao

    init {
        val db = DataBase.getInstance(context)
        db?.let {
            pokemonDao = it.pokemonDao()
        }
    }

    override suspend fun addPokemons(pokemons: List<PokemonDto>) {
        return pokemonDao.addPokemons(pokemons)
    }

    override suspend fun getAllPokemon(): List<PokemonDto> {
        return pokemonDao.getAllPokemon()
    }
}