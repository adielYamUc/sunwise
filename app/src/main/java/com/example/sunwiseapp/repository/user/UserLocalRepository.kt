package com.example.sunwiseapp.repository.user

import com.example.sunwiseapp.data.local.dto.UserDto
import com.example.sunwiseapp.models.User

class UserLocalRepository (private val dataSource: UserDataSource) {

    suspend fun getUser() : List<User> {
        return dataSource.getUser().map {
            User(it.email)
        }
    }

    suspend fun saveUser(email:String){
        dataSource.saveUser(UserDto(0, email))
    }

    suspend fun deleteUser(){
        dataSource.deleteUser()
    }

}