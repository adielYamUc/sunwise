package com.example.sunwiseapp.repository.user

import android.content.Context
import com.example.sunwiseapp.data.local.DataBase
import com.example.sunwiseapp.data.local.UserDao
import com.example.sunwiseapp.data.local.dto.UserDto

class UserDataSourceImp (context: Context) : UserDataSource {

    private lateinit var userDao: UserDao

    init {
        val db = DataBase.getInstance(context)
        db?.let {
            userDao = it.userDao()
        }
    }

    override suspend fun saveUser(user: UserDto) {
        return userDao.addUser(user)
    }

    override suspend fun getUser(): List<UserDto> {
        return userDao.getUser()
    }

    override suspend fun deleteUser() {
        return userDao.deleteAll()
    }
}