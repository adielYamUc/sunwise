package com.example.sunwiseapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.bumptech.glide.load.engine.Resource
import com.example.sunwiseapp.data.local.DbDataSource
import com.example.sunwiseapp.data.remote.PokemonRemoteDataSource
import com.example.sunwiseapp.models.Pokemon
import com.example.sunwiseapp.repository.PokemonLocalRepository
import com.example.sunwiseapp.viewmodel.PokemonVM
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class PokemonVmTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = CoroutineRule()

    @Mock
    private lateinit var remoteRepository: PokemonRemoteDataSource

    @Mock
    private lateinit var localRepository: DbDataSource

    @Mock
    private lateinit var pokemonObserver: Observer<List<Pokemon>>

    @Before
    fun setUp() {
        // do something if required
    }

    @Test
    fun givenServerResponse200_whenFetch_shouldReturnSuccess() {
        testCoroutineRule.runBlockingTest {
            doReturn(emptyList<Pokemon>())
                .`when`(remoteRepository)
                .getPokemos()
            /*val viewModel = PokemonVM(remoteRepository, localRepository)
            viewModel.getRemotePokemon()
            verify(remoteRepository).getPokemos()*/
        }
    }
}